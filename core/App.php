<?php
namespace Core;
    /**
    *
    */
    class App
    {

        function __construct()
        {
            require_once "../app/models/User.php";
            session_start();

            $url = isset($_REQUEST["url"]) ? $_REQUEST["url"] : "home";

            $argumemts = explode("/", trim($url,"/"));
            $controller = array_shift($argumemts) . "Controller";
            $controller = ucwords($controller);
            if(!count($argumemts)){
                $method = "index";
            }else{
                $method = array_shift($argumemts);
            }

            //echo $url;
            //echo "<pre>";
            //var_dump($argumemts);
            //echo $controller;
            //echo "<br>";
            //echo $method;

            $file = "../app/controllers/$controller" . ".php";
            if(file_exists($file)){
                //require_once $file;
            }else{
                header("HTTP/1.0 404 Not Found");
                echo "No encontrado";
                die();
            }

            $controller = "\\App\\Controllers\\" . $controller;
            $controllerObject = new $controller;
            if(method_exists($controller,$method)){
                try {
                    $controllerObject->$method($argumemts);
                } catch (\Exception $e) {
                    header("HTTP/1.0 500 Internal Error");
                    echo $e->getMessage();
                    echo "<pre>";
                    echo $e->getTraceAsString();
                }

            }else{
                header("HTTP/1.0 404 Not Found");
                echo "No encontrado";
                die();
            }
        }
    }
    ?>
