<?php
namespace App\Controllers;
//require_once "../app/models/User.php";
use \App\Models\User;
    /**
    *
    */
    class LoginController
    {

        function __construct()
        {
        }

        public function index(){
            if(isset($_SESSION["user"]) && !empty($_SESSION["user"])){
                require "../app/views/home.php";
            }else{
                require "../app/views/login.php";
            }
        }

        public function login()
        {
            $email = $_REQUEST["email"];
            $password = $_REQUEST["password"];
            $user = User::findByEmail($email);
            if($user != null && password_verify($password, $user->password)){

                $_SESSION["user"] = $user;
                $message = "Te has logeado correctamente.";

            }else{
                unset($_SESSION["user"]);
                $message = "Email o contraseña incorrectos.";
            }
            if(isset($_SESSION["user"]) && !empty($_SESSION["user"])){
                require "../app/views/home.php";
            }else{
                require "../app/views/login.php";
            }
        }

        public function logout()
        {
            session_destroy();
            require "../app/views/login.php";
        }
    }
    ?>
