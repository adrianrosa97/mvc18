<?php
namespace App\Controllers;
//require_once "../app/models/User.php";
use \App\Models\User;
    /**
    *
    */
    class UserController
    {

        function __construct()
        {
        }

        public function index(){
            $numero = 5;
            $users = User::paginate($numero);
            $rowCount = User::rowCount();

            $pages = ceil($rowCount / $numero);
            isset($_REQUEST["page"]) ? $page =(int) $_REQUEST["page"] : $page = 1;
            //$page = $_REQUEST["page"];
            require "../app/views/user/index.php";
        }

        public function show($args){
            $id = (int)$args[0];
            $user = User::find($id);
            require "../app/views/user/show.php";
        }

        public function create(){
            require "../app/views/user/create.php";
        }

        public function store(){

            $user = new User();
            $user->name = $_REQUEST["nombre"];
            $user->surname = $_REQUEST["apellidos"];
            $user->age = $_REQUEST["edad"];
            $user->email = $_REQUEST["email"];
            $user->insert();

            header("Location:/user");
        }

        public function delete($args){
            $id = (int)$args[0];
            $user = User::find($id);
            $user->delete();

            header("Location:/user");
        }

        public function edit($args){
            $id = (int)$args[0];
            $user = User::find($id);
            require "../app/views/user/edit.php";
        }

        public function update(){
            $id = $_REQUEST["id"];
            $user = User::find($id);
            $user->name = $_REQUEST["nombre"];
            $user->surname = $_REQUEST["apellidos"];
            $user->age = $_REQUEST["edad"];
            $user->email = $_REQUEST["email"];
            $user->save();
            header("Location:/user");
        }
    }
 ?>
