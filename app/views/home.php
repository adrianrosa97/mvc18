<!doctype html>
<html lang="es">
  <?php require "../app/views/parts/head.php" ?>
  <body>
    <?php require "../app/views/parts/header.php" ?>

    <main role="main" class="container">
      <br>
      <div class="starter-template">
        <h1>Aplicacion MVC</h1>
        <p class="lead"><?php
        if(isset($_SESSION["user"]) && !empty($_SESSION["user"])){
          $user = $_SESSION["user"];
          echo "Pagina de " . $user->name . ".";
        }else{
          echo "Necesitas logearte para ver tu home.";
        }
        ?></p>
      </div>

    </main>
    <?php require "../app/views/parts/footer.php" ?>
</body>
  <?php require "../app/views/parts/scripts.php" ?>
</html>


