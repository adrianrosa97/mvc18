<!doctype html>
<html lang="es">
  <?php require "../app/views/parts/head.php" ?>
  <body>
    <?php require "../app/views/parts/header.php" ?>

    <main role="main" class="container">
      <br>
      <div class="starter-template">
        <h1>Login</h1>
        <form action="/login/login" method="post">
            <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" class="form-control" name="email">
            </div>
            <div class="form-group">
                <label for="password">Contraseña:</label>
                <input type="password" class="form-control" name="password">
            </div>
            <button type="submit" class="btn btn-default">Logearse</button>
        </form>
        <h3 style="color: red;"><?php echo isset($message) && !empty($message) ? $message : "" ?></h3>
      </div>

    </main>
    <?php require "../app/views/parts/footer.php" ?>
</body>
  <?php require "../app/views/parts/scripts.php" ?>
</html>


