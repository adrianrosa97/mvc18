<!DOCTYPE html>
<html>
<?php require "../app/views/parts/head.php" ?>
<head>
    <title>Nuevo usuario</title>
</head>
<body>
    <?php require "../app/views/parts/header.php" ?>
    <main role="main" class="container">
      <br>
      <div class="starter-template">
        <form action="/user/store" method="post">
            <div class="form-group">
                <label for="nombre">Nombre:</label>
                <input type="text" class="form-control" name="nombre">
            </div>
            <div class="form-group">
                <label for="apellidos">Apellidos:</label>
                <input type="text" class="form-control" name="apellidos">
            </div>
            <div class="form-group">
                <label for="edad">Edad:</label>
                <input type="text" class="form-control" name="edad">
            </div>
            <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" class="form-control" name="email">
            </div>
            <div class="checkbox">
                <label><input type="checkbox"> Remember me</label>
            </div>
            <button type="submit" class="btn btn-default">Registrar</button>
        </form>
    </div>
</main>
<?php require "../app/views/parts/footer.php" ?>
</body>
<?php require "../app/views/parts/scripts.php" ?>
</html>
