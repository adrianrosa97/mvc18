<!DOCTYPE html>
<html>
<?php require "../app/views/parts/head.php" ?>
<head>
    <title>Edicion de usuario</title>
</head>
<body>
    <?php require "../app/views/parts/header.php" ?>
    <main role="main" class="container">
      <br>
      <div class="starter-template">
        <h1>Edicion de usuario</h1>
        <form action="/user/update" method="post">
            <input type="hidden" name="id" value="<?php echo $user->id ?>">
            <div class="form-group">
                <label for="nombre">Nombre:</label>
                <input type="text" class="form-control" name="nombre" value="<?php echo $user->name ?>">
            </div>
            <div class="form-group">
                <label for="apellidos">Apellidos:</label>
                <input type="text" class="form-control" name="apellidos" value="<?php echo $user->surname ?>">
            </div>
            <div class="form-group">
                <label for="edad">Edad:</label>
                <input type="text" class="form-control" name="edad" value="<?php echo $user->age ?>">
            </div>
            <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" class="form-control" name="email" value="<?php echo $user->email ?>">
            </div>
            <button type="submit" class="btn btn-default">Guardar</button>
        </form>
    </div>
</main>
<?php require "../app/views/parts/footer.php" ?>
</body>
<?php require "../app/views/parts/scripts.php" ?>
</html>
