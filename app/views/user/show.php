<!DOCTYPE html>
<html>
<?php require "../app/views/parts/head.php" ?>
<body>
    <?php require "../app/views/parts/header.php" ?>
    <main role="main" class="container">
      <br>
      <div class="starter-template">
        <h1>Detalles del usuario</h1>
        <ul>
            <li><?php echo "Id: " . $user->id ?></li>
            <li><?php echo "Nombre: " . $user->name ?></li>
            <li><?php echo "Apellidos: " . $user->surname ?></li>
            <li><?php echo "Edad: " . $user->age ?></li>
            <li><?php echo "Email: " . $user->email ?></li>
        </ul>
        <!-- <p><?php echo "Id: " . $user->id . "<br>Nombre: " . $user->name . "<br>Apellidos: " . $user->surname . "<br>Edad: " . $user->age . "<br>Email: " . $user->email?></p> -->
    </div>
</main>
<?php require "../app/views/parts/footer.php" ?>
</body>
<?php require "../app/views/parts/scripts.php" ?>
</html>
