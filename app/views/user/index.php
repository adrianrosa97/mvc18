<!doctype html>
<html lang="es">
<?php require "../app/views/parts/head.php" ?>
<body>
  <?php require "../app/views/parts/header.php" ?>

  <main role="main" class="container">
    <br>
    <div class="starter-template">
      <h1>Lista de usuarios</h1>
      <table class="table table-striped">
        <thead>
          <tr>
            <th>Id</th>
            <th>Nombre</th>
            <th>Apellidos</th>
            <th>Edad</th>
            <th>Email</th>
            <th>Acciones</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($users as $user): ?>
            <tr>
              <td><?php echo $user->id ?></td>
              <td><?php echo $user->name ?></td>
              <td><?php echo $user->surname ?></td>
              <td><?php echo $user->age ?></td>
              <td><?php echo $user->email ?></td>
              <!-- <td>
                <?php echo $user->birthdate ?> /
                <?php echo $user->dateOld ?> /
                <?php echo $user->date->format("d/m/Y") ?>
              </td> -->
              <td><a class="btn btn-primary" href="/user/show/<?php echo $user->id ?>">Ver</a>
               |
               <a class="btn btn-primary" href="/user/delete/<?php echo $user->id ?>">Borrar</a>
               |
               <a class="btn btn-primary" href="/user/edit/<?php echo $user->id ?>">Editar</a>
             </td>
           </tr>
         <?php endforeach ?>
       </tbody>
     </table>
     <hr>
     Paginas:
     <?php for ($i = 1;$i <= $pages;$i++){ ?>
     <?php if ($i != $page): ?>
     <a href="/user/index?page=<?php echo $i ?>" class="btn">
      <?php echo $i ?>
      </a>
   <?php else: ?>
    <span class="btn">
      <?php echo $i ?>
    </span>
  <?php endif ?>
  <?php } ?>
  <hr>
  <a href="/user/create">Nuevo usuario</a>
</div>
</main>
<?php require "../app/views/parts/footer.php" ?>
</body>
<?php require "../app/views/parts/scripts.php" ?>
</html>


